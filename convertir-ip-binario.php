<?php
    session_start();
    $ip = isset($_POST["ip"]) && $_POST["ip"] !='' ? $_POST["ip"] : null ;

    if(!is_null($ip)){
        if(filter_var($ip, FILTER_VALIDATE_IP)){
            $datos['mensaje'] = 'SUCCESS! Conversión exitosa';
            $octetos = explode(".", $ip);

            $byte1 =  str_pad(decbin(intval($octetos[0])), 8, "0", STR_PAD_LEFT);
            $byte2 =  str_pad(decbin(intval($octetos[1])), 8, "0", STR_PAD_LEFT);
            $byte3 =  str_pad(decbin(intval($octetos[2])), 8, "0", STR_PAD_LEFT);
            $byte4 =  str_pad(decbin(intval($octetos[3])), 8, "0", STR_PAD_LEFT);

            $datos['ip_binario'] = $byte1.'.'.$byte2.'.'.$byte3.'.'.$byte4;


        }else{
            $datos['mensaje'] = 'WARNING! Ingrese una IP valida';
        }
    }else{
    	$datos['mensaje'] = 'WARNING! Ingrese una dirección IP';
    }

    echo json_encode($datos);
    die();

    function isBinary($str) {
	    return preg_match('/^[01 ]+$/', $str) > 0;
	}
?>