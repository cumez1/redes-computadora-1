<!doctype html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Redes de computadoras I">
    <meta name="author" content="Nicolas Cúmez">
    <title>Redes de computadoras</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <style>
        #diagonal {
            width: 10px;
            padding: 0;
            border: none;
            font-size: 20px;
            padding-bottom: 10px;
            margin-top: 33px;
        }
    </style>
  </head>
  <body class="bg-light">
    <div class="container-fluid">
        <br>
        <br>
        <div class="row">
            <div class="col-md-4">
                <div class="card" style="min-height: 330px;">
                    <div class="card-body">
                        <h5 class="card-title">Convertir decimal a binario o vicerversa</h5>
                        <form id="form-conversion" autocomplete="off">
                            <div class="form-group">
                                <label for="decimal">Decimal</label>
                                <input type="text" class="form-control" id="decimal">
                            </div>
                            <div class="form-group">
                                <label for="binario">Binario <small id="lbl-lenght"></small></label>
                                <input type="text" class="form-control" id="binario">
                            </div>
                          <button type="submit" class="btn btn-primary">Convertir</button>
                          <button type="reset" id="btn-reset" class="btn btn-danger">Limpiar</button>

                          <pre style="padding-top: 10px">NOTA: Se calculará conforme el orden de datos ingresados</pre>
                        </form>
                    </div>
                </div>
            </div>

            <div class="col-md-3">
                <div class="card" style="min-height: 330px;">
                    <div class="card-body">
                        <h5 class="card-title">Resultados</h5>
                        <span>DECIMAL: <pre style="font-size: 25px" id="lbl-decimal"></pre></span>
                        <span>BINARIO: <pre style="font-size: 25px" id="lbl-binario"></pre></span> <br>
                        <pre id="lbl-mensaje"></pre>
                    </div>
                </div>
            </div>

            <div class="col-md-5">
                <div class="card" style="min-height: 330px;">
                    <div class="card-body">
                        <h5 class="card-title">Convertir IP a binario</h5>
                        <form id="form-ip-binario" autocomplete="off">
                            <div class="form-group">
                                <label for="ip">IP</label>
                                <input type="text" class="form-control" id="ip">
                            </div>
                          <button type="submit" class="btn btn-primary">Convertir</button>
                          <button type="reset" id="btn-reset" class="btn btn-danger">Limpiar</button>

                          <pre id="lbl-ip-binario" style="padding-top: 20px; font-size: 25px;"></pre>
                          <pre id="lbl-ip-binario-mensaje"></pre>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-8">
                <div class="card" style="min-height: 330px;">
                    <div class="card-body">
                        <h5 class="card-title">Subnetting VLSM</h5>
                        <pre>Máscaras de Subred de Longitud Variable</pre>
                        <form id="form-subnetting-vlsm" autocomplete="off">
                            <div class="form-row">
                                <div class="form-group col-md-4">
                                    <label for="ip_vlsm">IP</label>
                                    <input type="text" class="form-control" id="ip_vlsm">
                                </div>
                                <span id="diagonal">/</span>
                                <div class="form-group col-md-2">
                                    <label for="bloque_cidr">Bloque CIDR</label>
                                    <input type="text" class="form-control" id="bloque_cidr">
                                </div>
                                <div class="form-group col-md-2">
                                    
                                    <button style="margin-top: 32px" type="submit" class="btn btn-primary">Calcular</button>
                                </div>
                                
                            </div>
                          <pre id="lbl-vlsm" style="padding-top: 20px; font-size: 25px;"></pre>
                          <pre id="lbl-vlsm-mensaje"></pre>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="js/jquery-3.4.1.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>


    <script type="text/javascript">
        $(document).ready(function () {
            $('#binario' ).keyup(function(event) {
                $('#lbl-lenght').text('Longitud: '+$('#binario').val().length);
            });

            $('#form-conversion').submit(function(event) {
                event.preventDefault();
                limpiarConversion();
                convertidor();
            });

            $('#form-ip-binario').submit(function(event) {
                event.preventDefault();
                convertidorIpBinario();
            });

            $('#form-subnetting-vlsm').submit(function(event){
                event.preventDefault();
                calcularVLSM();
            });

            $('#btn-reset').click(function(event) {
                $('#lbl-lenght').empty();
                limpiar();
            });

            $('#btn-reset').click(function(event) {
                limpiarIpBinario();
            });
           
        });

        function convertidor() {
            let decimal = $('#decimal').val();
            let binario = $('#binario').val();
            let URL = 'convertidor.php';
            let data =  {
                            decimal: decimal, 
                            binario: binario
                        };

            callAjax(URL,data, function (response) {
                $('#lbl-decimal').text(response.decimal);
                $('#lbl-binario').text(response.binario);
                $('#lbl-mensaje').text(response.mensaje);
            });
        }

        function convertidorIpBinario() {
            let ip = $('#ip').val();
            let URL = 'convertir-ip-binario.php';
            let data =  { ip: ip };

            callAjax(URL,data, function (response) {
                $('#lbl-ip-binario').text(response.ip_binario);
                $('#lbl-ip-binario-mensaje').text(response.mensaje);
            });
        }

        function calcularVLSM() {
            let ip_vlsm = $('#ip_vlsm').val();
            let bloque_cidr = $('#bloque_cidr').val();

            let URL = 'calcular-vlsm.php';
            let data =  { ip_vlsm: ip_vlsm ,
                          bloque_cidr: bloque_cidr  
                        };

            callAjax(URL,data, function (response) {
                $('#lbl-vlsm').text(response.vlsm);
                $('#lbl-vlsm-mensaje').text(response.mensaje);
            });
        }


        function callAjax(url, datos, response){
            $.ajax({
                type: "POST",
                url: url,
                dataType : 'json',
                data: datos,
                success: response,
                error:response           
            });                
        }

        function limpiarConversion() {
            $('#lbl-decimal').empty();
            $('#lbl-binario').empty();
            $('#lbl-mensaje').empty();
        }

        function limpiarIpBinario() {
            $('#lbl-ip-binario').empty();
            $('#lbl-ip-binario-mensaje').empty();
        }
    </script>

</body>
</html>
