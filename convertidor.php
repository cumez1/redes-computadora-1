<?php
    session_start();
    $decimal = isset($_POST["decimal"]) && $_POST["decimal"] !='' ? $_POST["decimal"] : null ;
    $binario = isset($_POST["binario"]) && $_POST["binario"] !='' ? $_POST["binario"] : null ;


    if(!is_null($decimal)){
    	$datos['decimal'] = $decimal;
    	$datos['binario'] = decbin($decimal);
    	$datos['mensaje'] = 'Conversión exitosa DEC - BIN';

    }else{
    	if(!is_null($binario) && isBinary($binario)){
	    	$datos['decimal'] = bindec($binario);
	    	$datos['binario'] = $binario;
	    	$datos['mensaje'] = 'Conversión exitosa BIN - DEC';
	    }else{
	    	$datos['mensaje'] = 'La cadena ingresada no es un binario';
	    }
    }

    echo json_encode($datos);
    die();

    function isBinary($str) {
	    return preg_match('/^[01 ]+$/', $str) > 0;
	}
?>