<?php
    session_start();
    $ip = isset($_POST["ip_vlsm"]) && trim($_POST["ip_vlsm"]) !='' ? trim($_POST["ip_vlsm"]) : null ;
    $bloque = isset($_POST["bloque_cidr"]) && trim($_POST["bloque_cidr"]) !='' ? trim($_POST["bloque_cidr"]) : null ;

    if(is_null($ip) || is_null($bloque)){
        $datos['mensaje'] = 'WARNING! Ingrese una dirección IP ó verifique el bloque CIDR';
        echo json_encode($datos);
        die();
    }

    if($bloque < 1 || $bloque > 32){
        $datos['mensaje'] = 'WARNING! El bloque CIDR tiene que estar en el rango [1 - 32]';
        echo json_encode($datos);
        die();
    }

    if(!is_null($ip)){
        if(filter_var($ip, FILTER_VALIDATE_IP)){
            
        }else{
            $datos['mensaje'] = 'WARNING! Ingrese una IP valida';
        }
    }else{
    	$datos['mensaje'] = 'WARNING! Ingrese una dirección IP';
    }

    echo json_encode($datos);
    die();

    function isBinary($str) {
	    return preg_match('/^[01 ]+$/', $str) > 0;
	}


?>